app.controller('flickrPhotosController', function($scope, $http, API_URL) {
	//fetch recent photos from flickr using API
	$scope.loading = true;
	$http.get(API_URL + "recent_photos")
	.then(function successCallback(response) {
		$scope.photos = response.data.photos;
		$scope.stat = response.data.stat;
		$scope.message = response.data.message;
		$scope.loading = false;
	},function errorCallback(response) {});
});

