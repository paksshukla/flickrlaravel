app.controller('flickrSizeController', function($scope, $http, API_URL) {
    //get image information and different sizes available
    $scope.loading = true;
    $http.get(API_URL + "size/" + photo_id)
    .then(function successCallback(response) {
        $scope.sizes = response.data.sizes;
        $scope.photo = response.data.photo;
        $scope.stat = response.data.stat;
        $scope.message = response.data.message;
        $scope.loading = false;
    },function errorCallback(response) {});
});
