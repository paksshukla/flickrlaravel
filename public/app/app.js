var app = angular.module('flickrPhotos', [], function($interpolateProvider) {
        
        // start and end symbol changed because '{{' and '}}' are used by both AngularJS and BladeTemplate
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');

}).constant('API_URL', 'https://localhost/flickrlaravel/public/');
