@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('content')
    @include('includes.header')

    <div class="row" ng-controller="flickrPhotosController">
	    <div class="col-md-12" ng-show="loading">
	        <img src="<?= asset('images/loader.gif') ?>">
	    </div>

	    <div ng-if="stat == 'ok'" >
	        <span ng-repeat="photo in photos">
	            <a target="_blank" href="image_size/<% photo.id %>">
	                <img src="https://farm<% photo.farm %>.staticflickr.com/<% photo.server %>/<% photo.id %>_<% photo.secret %>.jpg" width="200" />
	            </a>
	        </span>
	    </div>
	    <div ng-if="stat != 'ok'" class="col-md-12">
            <% message %>
        </div>
    </div>
@endsection
