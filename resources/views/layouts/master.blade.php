<!DOCTYPE html>
<html lang="en-US" ng-app="flickrPhotos">
    <head>
        <title>@yield('title')</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
    <div class="container">
    	@yield('content')
    </div>

    <!-- Load AngularJS Library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.0/angular.min.js"></script>
    
    <!-- AngularJS Application Scripts -->
    <script src="<?= asset('app/app.js') ?>"></script>
    <script src="<?= asset('app/controllers/flickrPhotos.js') ?>"></script>
    <script src="<?= asset('app/controllers/flickrSize.js') ?>"></script>
    </body>

</html>
