@extends('layouts.master')

@section('title')
    Page Not Found
@endsection

@section('content')
    @include('includes.header')

     <div class="row">
        <div class="col-md-12">
            <h1>500! Internal Server Error!</h1>
        </div>
    </div>
@endsection
