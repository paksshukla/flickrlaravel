@extends('layouts.master')

@section('title')
    Page Not Found
@endsection

@section('content')
    @include('includes.header')

     <div class="row">
        <div class="col-md-12">
            <h1>404! Page not found!</h1>
        </div>
    </div>
@endsection
