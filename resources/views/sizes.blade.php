@extends('layouts.master')

@section('title')
    Photo Size
@endsection

@section('content')
    @include('includes.header')

     <div class="row" ng-controller="flickrSizeController">
        <div class="col-md-12" ng-show="loading">
            <img src="<?= asset('images/loader.gif') ?>">
        </div>

        <div ng-if="stat == 'ok'" >
            <div class="col-md-6">
                <img src="https://farm<% photo.farm %>.staticflickr.com/<% photo.server %>/<% photo.id %>_<% photo.secret %>.jpg" max-width="600" />
            </div>

            <div class="col-md-6">
                <p><strong>Available Sizes</strong></p>
                <span ng-repeat="size in sizes">
                    <p><a target="_blank" href="<% size.source %>"><% size.label %> (<% size.width %> x <% size.height %>)</a></p>
                </span>

            </div>
        </div>
        <div ng-if="stat != 'ok'" class="col-md-12">
            <% message %>
        </div>
    </div>
@endsection

<script type="text/javascript">
    var photo_id = {{ $id }};
</script>
