<?php

namespace App\Http\Controllers;

use App\Libraries\FlickrAPI;

class flickrController extends Controller
{
    /**
     * Calls flick API to get recent photos.
     *
     * @return view of recent photos
     */
    public function recentPhotos()
    {
        $flickr = new FlickrAPI;
        //to get recent photos from flickr api
        $res = $flickr->curlRequest( $flickr->getRecentPhotoUrl() );
        $res = json_decode($res);
 
        if($res->stat == 'ok') {
            return ['stat' => $res->stat, 'photos' => $res->photos->photo ];
        }
        else {
            return ['stat' => $res->stat, 'code' => $res->code, 'message' => $res->message ];
        }
    }


    /**
     * Loads view for displaying different sizes of photo.
     *
     * @param id
     * @return view of sizes
     */
    public function loadSizeView($id)
    {
        return view('sizes',['id' => $id]);
    }


    /**
     * Calls flickr API to get different size of photo
     *
     * @param id
     * @return json with different available sizes of photo selected
     */
    public function imagesize($id)
    {
        $flickr = new FlickrAPI;
        //get the information of the photo
        $infoRes = json_decode($flickr->curlRequest( $flickr->getImageInfoUrl($id) ));
        
        if($infoRes->stat == 'ok') {
            //get the different available sizes for the photo
            $sizeRes = json_decode($flickr->curlRequest( $flickr->getSizeUrl($id) ));

            if($sizeRes->stat == 'ok') {
                return ['stat' => $sizeRes->stat, 'sizes' => $sizeRes->sizes->size, 'photo' => $infoRes->photo];
            }
            else {
                return ['stat' => $sizeRes->stat, 'code' => $sizeRes->code, 'message' => $sizeRes->message ];
            }
        }
        else {
            return ['stat' => $infoRes->stat, 'code' => $infoRes->code, 'message' => $infoRes->message ];
        }
    }

}
