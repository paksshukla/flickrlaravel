<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
	return view('welcome');
});

Route::get('/recent_photos', [
	'uses' => 'flickrController@recentPhotos',
	'as' => 'flickr.recentPhotos'
]);

Route::get('/image_size/{id}', [
	'uses' => 'flickrController@loadSizeView',
	'as' => 'flickr.sizeView'
]);

Route::get('/size/{id}', [
	'uses' => 'flickrController@imagesize',
	'as' => 'flickr.imagesize'
]);
