<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Config;

class FlickrAPI
{
    /**
     * Fires curl request to flickr api and return the response in required(json) format.
     *
     * @param url
     * @return json response of curl request
     */
    public function curlRequest($url)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
        catch(Exception $e) {
            return json_encode(array('error' => $e->getMessage()));
        }
    }


    /**
     * Generate URL for recent photo flickr API.
     *
     * @return recentPhotoUrl
     */
    public function getRecentPhotoUrl()
    {
        $post_data = [
            'api_key' => Config::get('flickrConstants.key'),
            'per_page' => Config::get('flickrConstants.perPage'),
            'format' => Config::get('flickrConstants.format'),
            'nojsoncallback' => 1
        ];
        return $url = Config::get('flickrConstants.recentPhotoUrl').'&'.http_build_query($post_data);
    }


    /**
     * Generate URL for image information flickr API.
     *
     * @return imageInfoUrl
     */
    public function getImageInfoUrl($photoid)
    {
        $post_data = [
            'api_key' => Config::get('flickrConstants.key'),
            'photo_id' => $photoid,
            'format' => Config::get('flickrConstants.format'),
            'nojsoncallback' => 1
        ];
        return $url = Config::get('flickrConstants.imageInfoUrl').'&'.http_build_query($post_data);
    }


    /**
     * Generate URL for size flickr API.
     *
     * @return getSizeUrl
     */
    public function getSizeUrl($photoid)
    {
        $post_data = [
            'api_key' => Config::get('flickrConstants.key'),
            'photo_id' => $photoid,
            'format' => Config::get('flickrConstants.format'),
            'nojsoncallback' => 1
        ];
        return $url = Config::get('flickrConstants.getSizeUrl').'&'.http_build_query($post_data);
    }

}
