<?php

return [

    /*
    *
    * flicker App Name
    *
    */
    'appName' => 'NetWebPrakash',


    /*
    *
    * flicker Key
    *
    */
    'key' => '75a2a6719694cec00c2fd7efff9dfc54',


    /*
    *
    * flicker Secret
    *
    */
    'secret' => '34cff7629e6d6085',


    /*
    *
    * flicker API URL
    *
    */
    'url' => 'https://www.flickr.com/services/oauth/',


    /*
    *
    * flicker API URL for request token
    *
    */
    'reqTokenUrl' => 'https://www.flickr.com/services/oauth/request_token',


    /*
    *
    * flicker API URL for authorization
    *
    */
    'authorizeUrl' => 'https://www.flickr.com/services/oauth/authorize',


    /*
    *
    * flicker API URL for access token
    *
    */
    'accessTokenUrl' => 'https://www.flickr.com/services/oauth/access_token',


    /*
    *
    * flicker API URL for recent photos
    *
    */
    'recentPhotoUrl' => 'https://api.flickr.com/services/rest/?method=flickr.photos.getRecent',


    /*
    *
    * flicker API URL for different sizes of photo
    *
    */
    'getSizeUrl' => 'https://api.flickr.com/services/rest/?method=flickr.photos.getSizes',


    /*
    *
    * flicker API URL for getting image information
    *
    */
    'imageInfoUrl' => 'https://api.flickr.com/services/rest/?method=flickr.photos.getInfo',
    

    /*
    *
    * flicker API permission
    *
    */
    'permission' => 'read',
    

    /*
    *
    * Recenet photos per page
    *
    */
    'perPage' => 50,
    

    /*
    *
    * format of flickr API response
    *
    */
    'format' => 'json',
];
